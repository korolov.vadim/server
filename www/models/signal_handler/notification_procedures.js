module.exports = {

    send_crawler_stats: (message, clients) => {
//        console.log(message);
        let access_level = message.access_level;

        for (var client_socket_id in clients) {
            if (clients.hasOwnProperty(client_socket_id)) {
                let client = clients[client_socket_id];

                if (access_level.includes(client.handshake.session.userrole)){
                    client.emit("notification_crawler_stats", message.data);
                }
            }
        }
    },

    send_new_search_results: (message, clients) => {
       console.log(message);
        let access_level = message.access_level;

        for (var client_socket_id in clients) {
            if (clients.hasOwnProperty(client_socket_id)) {
                let client = clients[client_socket_id];

                if (access_level.includes(client.handshake.session.userrole) && client.handshake.session.userid === message.data.user_id){
                    console.log(message.data.new_search_results);
                    client.emit("notification_new_search_results", message.data.new_search_results);
                }
            }
        }
    }
}