module.exports = {

    user_login: (message, b_client) => {
//        console.log(message);
        if (message.payload.user_id === 0) {
            b_client.emit("authorization_failed", {});
        } else {
            let user_id = message.payload.user_id;
            let user_role = message.payload.user_role;
            // console.log(user_id, user_role)
//            console.log("USER ROLE: ", user_role);
            let dest = '';
            if (user_role === "admin") {
                dest = "/admin/dashboard.html";
            } else {
                dest = "/index.html"
            }
           // console.log(b_client)
            b_client.handshake.session.userid = user_id;
            b_client.handshake.session.userrole = user_role;
            b_client.handshake.session.save();
            // console.log(dest);
            b_client.emit("redirect", {"dest": dest})
        }
    },
    user_follow_query: (message, b_client) => {
       console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("user_follow-query", {
                "metadata": {},
                "payload": message.payload
            });
        }
    },
    user_unfollow_query: (message, b_client) => {
       console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("user_unfollow-query", {
                "metadata": {},
                "payload": message.payload
            });
        }
    },
    user_reset_search_notification: (message, b_client) => {
       console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("user_reset-search-notification", {
                "metadata": {},
                "payload": message.payload
            });
        }
    },
    search_query_get_search_results: (message, b_client) => {
       console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("search-query_get-search-results", {
                "metadata": {},
                "payload": message.payload
            });
        }
    },
    search_query_search: (message, b_client) => {
       console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("search-query_search", {
                "metadata": {},
                "payload": message.payload
            });
        }
    },
    search_query_search_followed: (message, b_client) => {
       console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("search-query_search-followed", {
                "metadata": {},
                "payload": message.payload
            });
        }
    },
    crawler_create: (message, b_client) => {
//        console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            crawler_id = message.payload.crawler_id;
            b_client.emit("crawler_created", {"crawler_id": crawler_id});
        }
    },
    crawler_update: (message, b_client) => {
//        console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            crawler_id = message.payload.crawler_id;
            b_client.emit("crawler_updated", {"crawler_id": crawler_id});
        }
    },
    crawler_search: (message, b_client) => {
       console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("crawler_searched", {"search_results": message.payload});
        }
    },
    crawler_get: (message, b_client) => {
//        console.log(message);
        if (message.payload === 0) {
            b_client.emit("command_failed", {});
        } else {
            b_client.emit("crawler_got", {"crawler_data": message.payload});
        }
    },
    crawler_test: (message, b_client) => {
        // console.log("Test crawler procedures MASTERPAGE", message);
        if (message.payload === "1"){
            b_client.emit("test_completed", message.payload);
        } else {
            let signal = [message.payload.object, "_crawled"].join('');
            b_client.emit(signal, message.payload);
        }
    }
}