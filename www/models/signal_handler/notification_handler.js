const notification_procedures_js = require('./notification_procedures.js');


class NotificationHandler {
    constructor () {
        let test = true;
    }

    process_signal (message, clients) {
        let notification_name = message.notification_name;

        if (notification_name === "crawler_stats") {
            notification_procedures_js.send_crawler_stats(message, clients);

        } else if (notification_name === "new_search_results"){
            notification_procedures_js.send_new_search_results(message, clients);

        }
    }
}

module.exports = NotificationHandler