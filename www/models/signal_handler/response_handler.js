const response_procedures = require('./response_procedures.js');


class ResponseHandler {
    constructor () {
        let test = true;
    }

    process_signal (message, b_client) {
        let obj = message.object;
        let action = message.action;
        let func_name = obj.concat('_', action);

        if (func_name === "user_login") {
            response_procedures.user_login(message, b_client);

        } else if (func_name === "user_follow-query") {
            response_procedures.user_follow_query(message, b_client);

        } else if (func_name === "user_unfollow-query") {
            response_procedures.user_unfollow_query(message, b_client);

        } else if (func_name === "user_reset-search-notification") {
            response_procedures.user_reset_search_notification(message, b_client);

        } else if (func_name === "search-query_get-search-results") {
            response_procedures.search_query_get_search_results(message, b_client);

        } else if (func_name === "search-query_search") {
            response_procedures.search_query_search(message, b_client);

        } else if (func_name === "search-query_search-followed") {
            response_procedures.search_query_search_followed(message, b_client);

        } else if (func_name === "crawler_test") {
            response_procedures.crawler_test(message, b_client);

        }  else if (func_name === "crawler_create") {
            response_procedures.crawler_create(message, b_client);

        } else if (func_name === "crawler_update") {
            response_procedures.crawler_update(message, b_client);

        } else if (func_name === "crawler_search") {
            response_procedures.crawler_search(message, b_client);

        } else if (func_name === "crawler_get") {
            response_procedures.crawler_get(message, b_client);
        }
    }
}

module.exports = ResponseHandler