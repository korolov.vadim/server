import {Consumer} from "kafkajs";

const { Kafka } = require('kafkajs');
export class IKafka {
    #kafka:any
    #consumer:any
    #producer:any

    #events = {
        connected : []
    }
    #is =  {
        connected : false
    }
    constructor(brokers:string[] | any[], clientId:string) {
        this.#kafka = new Kafka({
            clientId : clientId,
            brokers  : !brokers[0]?[brokers]:brokers
        });
        this.#producer = this.#kafka.producer();
        this.#producer.connect().then(async () => {
            this.#events.connected.forEach((cb) => {
                cb();
            });

            //const admin = this.#kafka.admin()
            //await admin.connect();

            this.#is.connected = true;
        })
    }
    async subscribe(topic:object | any, fromBeginning = true, groupId:string, eachMessage?:Function):Promise<Consumer>{
        let consumer:Consumer = this.#kafka.consumer({ groupId: groupId });

        await consumer.connect();
        await consumer.subscribe({
                topic         : topic,
                fromBeginning : fromBeginning
            } as any
        );

        (() => {
            consumer.run({
                eachMessage : async ({ topic, partition, message }) => {
                    if(eachMessage) eachMessage(topic, partition, message)
                }
            } as any);
        })() ;
        return consumer;
    }
    async send(topicName : string, messages:any[] | string ){
        await this.#producer.send({
            topic: topicName,
            messages: messages,
        } as any)
    }
    on(eventName,cb){
        this.#events[eventName].push(cb);
    }
    static utils = {
        uuid : () => {
            return Date.now().toString(36) + Math.random().toString(36).substring(2)
        }
    }
}