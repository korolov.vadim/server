//
const ResponseHandler = require('./www/models/signal_handler/response_handler.js'),
      response_handler = new ResponseHandler(),
      NotificationHandler = require('./www/models/signal_handler/notification_handler.js'),
      notification_handler = new NotificationHandler(),
      express    = require('express'),
      app        = express(),
      bodyParser = require('body-parser'),
     cookieParser= require("cookie-parser"),
      http       = require('http'),
      io         = require("socket.io").Server,
      client     = require("socket.io-client"),
      server     = http.createServer(app) ,
      serverIO   = new io(server),
      Kafka      = require('./kafka').IKafka,
      SIO        = require("express-socket.io-session"),
      session = require("express-session")({
           secret: "keyboard cat",
           resave: true,
           saveUninitialized: true,
           cookie: { maxAge: 3600000 }
     })

/**
 *
 * @type {{port: number, ip: string}}
 */
// const gateways = {
//     server: {
//         ip  : '192.168.2.190',
//         port: 3000
//     },
//     kafka : {
//         brokers  : ['192.168.2.190:9092'],
//         clientId : Kafka.utils.uuid(),
//         groupId  : Kafka.utils.uuid()
//     }
// }

const gateways = {
    server: {
        ip  : '10.0.0.74',
        port: 3000
    },
    kafka : {
        brokers  : ['10.0.0.74:9092'],
        clientId : Kafka.utils.uuid(),
        groupId  : Kafka.utils.uuid()
    }
}

/**
 *
 * @type {IKafka}
 */
let kafka = new Kafka(
    gateways.kafka.brokers,
    gateways.kafka.clientId
);


/**
 *
 * @type {(session|*)[]}
 */
let plugins = [
    cookieParser(),
    session,
    bodyParser.json(),
    bodyParser.urlencoded({
        extended: true
    })
];



plugins.forEach(plugin => app.use(plugin));

serverIO.use(SIO(session, {
    autoSave:true
}));

/*app.use(session);

// Use shared session middleware for socket.io
// setting autoSave:true
serverIO.use(SIO(session, {
    autoSave:true
}));


app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));*/


/**
 *
 * @type {({admin: {server: *, objects: [string, string, string]}}|{main: {server: *, objects: string[]}}|{tester: {server: *, objects: []}})[]}
 */

// let scopes = [{
//     main   : {
//         objects : [
//             'user_update',
//             'user_login',
//             'search_semantic',
//             'user_create',
//             'crawler_create',
//             'crawler_update',
//             'crawler_get',
//             'crawler_search',
//             'crawler_test'
//         ]
//     }
// }];

let scopes = [
            'user_update',
            'user_login',
            'user_create',
            'user_follow-query',
            'user_unfollow-query',
            'user_reset-search-notification',
            'search-query_get-search-results',
            'search-query_search',
            'search-query_search-followed',
            'crawler_create',
            'crawler_update',
            'crawler_get',
            'crawler_search',
            'crawler_test'
        ];

global.clients = {};

kafka.on('connected', async () => {
    await kafka.subscribe('outbound_gateway',false,'outbound_gateway' + Kafka.utils.uuid(),(topic, partition, message) => {
        let _message = JSON.parse(message.value.toString());
        console.log('RESPONSE TO CLIENT =>',  _message);
        // console.log(_message.client_socket_id)
        let b_client = clients[_message.client_socket_id];
        response_handler.process_signal(_message, b_client);
    });

    await kafka.subscribe('notification_gateway',false,'notification_gateway' + Kafka.utils.uuid(),(topic, partition, message) => {
        let _message = JSON.parse(message.value.toString());
        console.log('NOTIFICATION =>',  _message);
        // console.log(_message.client_socket_id)
        // let b_client = clients[_message.client_socket_id];
        notification_handler.process_signal(_message, clients);
    });

    serverIO.on('connection', function (socket)  {
        console.log('Browser client connected: ', socket.id);

        // socket.session.userid = socket.id;
        // socket.seesion.save();

        clients[socket.id] = socket;
        //console.log(clients[socket.id]);
        // console.log(serverIO.sockets.sockets[socket.id])
        socket.on("signal", function (message) {
            // scope value should be one of those values : [admin, main, tester]
            message.client_socket_id = socket.id;
            console.log('REQUEST FROM CLIENT =>',  message);
            // let scope = scopes
            //     .filter(x => x[Object.keys(x)[0]]['objects'].indexOf([message.object,message.action].join('_')) !== -1)[0];

            // console.log(scope);
            let object = message.object;
            let action = message.action;
            let object_action = object.concat('_', action);

            if(scopes.includes(object_action)) {
                if (socket.handshake.session.userid) {
                    message.params.user_id = socket.handshake.session.userid;
                    // console.log(message)
                    kafka.send('inbound_gateway',[{
                        value : JSON.stringify(message),
                        key   : Kafka.utils.uuid()
                    }]);
                } else {
                    if (object_action !== 'user_login') {
                        // console.log(message);
                        console.log('Redirected')
                        socket.emit("redirect", {"dest": '../login.html'})
                    }
                }
            } else {
                console.log(`ERROR: Object ${object} and action ${action} is not served by any available app.`);
            }

        });
    });


    app.use('/admin/*',function (req,res,next){
        // console.log('req',req)
        if (!req.session.userid) {
            res.redirect('../login.html');
        } else {
            next();
        }
    });

    app.use(express.static('www'));
    app.get('/favicon.ico', (req, res) => res.status(204));

    server.listen(gateways.server.port,gateways.server.ip,() => {
        console.log(`Server is mounted at ${gateways.server.ip}:${gateways.server.port}`)
    });
});