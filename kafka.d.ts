import { Consumer } from "kafkajs";
export declare class IKafka {
    #private;
    constructor(brokers: string[] | any[], clientId: string);
    subscribe(topic: object | any, fromBeginning: boolean, groupId: string, eachMessage?: Function): Promise<Consumer>;
    send(topicName: string, messages: any[] | string): Promise<void>;
    on(eventName: any, cb: any): void;
    static utils: {
        uuid: () => string;
    };
}
