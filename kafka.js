"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var _kafka, _consumer, _producer, _events, _is;
Object.defineProperty(exports, "__esModule", { value: true });
const { Kafka } = require('kafkajs');
class IKafka {
    constructor(brokers, clientId) {
        _kafka.set(this, void 0);
        _consumer.set(this, void 0);
        _producer.set(this, void 0);
        _events.set(this, {
            connected: []
        });
        _is.set(this, {
            connected: false
        });
        __classPrivateFieldSet(this, _kafka, new Kafka({
            clientId: clientId,
            brokers: !brokers[0] ? [brokers] : brokers
        }));
        __classPrivateFieldSet(this, _producer, __classPrivateFieldGet(this, _kafka).producer());
        __classPrivateFieldGet(this, _producer).connect().then(() => __awaiter(this, void 0, void 0, function* () {
            __classPrivateFieldGet(this, _events).connected.forEach((cb) => {
                cb();
            });
            //const admin = this.#kafka.admin()
            //await admin.connect();
            __classPrivateFieldGet(this, _is).connected = true;
        }));
    }
    subscribe(topic, fromBeginning = true, groupId, eachMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            let consumer = __classPrivateFieldGet(this, _kafka).consumer({ groupId: groupId });
            yield consumer.connect();
            yield consumer.subscribe({
                topic: topic,
                fromBeginning: fromBeginning
            });
            (() => {
                consumer.run({
                    eachMessage: ({ topic, partition, message }) => __awaiter(this, void 0, void 0, function* () {
                        if (eachMessage)
                            eachMessage(topic, partition, message);
                    })
                });
            })();
            return consumer;
        });
    }
    send(topicName, messages) {
        return __awaiter(this, void 0, void 0, function* () {
            yield __classPrivateFieldGet(this, _producer).send({
                topic: topicName,
                messages: messages,
            });
        });
    }
    on(eventName, cb) {
        __classPrivateFieldGet(this, _events)[eventName].push(cb);
    }
}
exports.IKafka = IKafka;
_kafka = new WeakMap(), _consumer = new WeakMap(), _producer = new WeakMap(), _events = new WeakMap(), _is = new WeakMap();
IKafka.utils = {
    uuid: () => {
        return Date.now().toString(36) + Math.random().toString(36).substring(2);
    }
};
